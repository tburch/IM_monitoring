################################################
#             IM Spy Buffer Check              #
#          tyler.james.burch@cern.ch           #
################################################

# Run as:
# python IMbufferCheck.py -f1 InputFile.dat -o Output.pdf(optional)
# InputFile.dat may be an IM spy buffer or a Test Vector
from ROOT import *
import sys
import linecache
import difflib
import argparse


parser = argparse.ArgumentParser('Check for consistencies within a file')
parser.add_argument('-f1', dest='file1', required=True,
                    help='Input files 1')
parser.add_argument('-o', dest='outputPDF', required=False,
                    help='output pdf name',default='nWords.pdf')
args = parser.parse_args()

sample = args.file1
f = open(sample,'r')

# Create Histograms for inSpy and outSpy
histos={}
for channel in range(16):
    name = "inSpy_nWords_ch{0}".format(channel)
    histos[name]  = TH1F(name,"",501,0,500)

for channel in range(16):
    name = "outSpy_nWords_ch{0}".format(channel)
    histos[name]  = TH1F(name,"",501,0,500)



def idIncCheck(lastL1,thisL1):
    """Converts l1ID's to decimal and makes sure they're increasing monotonically"""

    if lastL1 == -1:
        #First Event for this channel
        return False
    if lastL1 == 0:
        #print last L1ID not valid
        return False
    elif int(lastL1,16)+1 != int(thisL1,16):
        print "L1ID did not increase by 1"
        print "    Last L1: ", lastL1
        print "    This L1: ", thisL1
        return True
    else: return False


def formatLine(line):
    """ Turns spy buffer lines into 8 char hex"""
    line = line.split("-")[1].split()[0][2:]
    return line

def inDetConsistencyCheck(content):
    """ Checks for consistency in inner Detector formatted data (inSpy) """
    header, data, event,channelActive = False, False, False, False
    head_l1,nWords, lastL1 = 0,0,-1
    nGoodEvents,nBadEvents, channelFails= 0,0,0
    channel = 0
    print "-----Ch 0-----"

    for i,line in enumerate(content):
        if "[FTK_IM" in line and "inspy" in line:
            thisChannel = line.split("inspy")[1].split()[0][2:][:-1]

            # Conditions if we move to a new channel
            if channel != int(thisChannel):
                if not channelActive: print "Channel Inactive"
                if channelActive: print "Number of issues in channel: ", channelFails
                event,channelActive = False,False
                channelFails = 0
                channel +=1
                lastL1 = -1
                print "-----Ch {0}-----".format(channel)


            line = line.split("-")[1].split()[0][2:] # Skim extra contents out of line
            FAIL = False

            # Check if a channel is active based on *any* nonzero events
            if channelActive is not True and not "00000000" in line: channelActive = True

            if line.startswith("b0f00000"): # start of event
                nDataWords,data = 0,False
                if event == True: # Throw error statements if already in an event (no fail, start new event here)
                    print "b0f came in middle of event!"
                    print "    line number: ", i,"\n    l1ID: ", head_l1
                header,event = True,True
                headerLength = int(formatLine(content[i+2]),16)
                headerWordSeen = 0
                head_l1 =  formatLine(content[i+6]) # Get header l1id based on position wrt b0f
            if header:
                headerWordSeen +=1
                if headerWordSeen is headerLength+2:
                    header,data = False,True
            if event:
                if data:
                    nWords+=1 # Add nWord for every word in data

                if line.startswith("e0f00000"): # End of event
                    FAIL = idIncCheck(lastL1,head_l1)  # Fails if l1id doesn't increase right, doesn't fail if previous was bad
                    nStatusWords = int(formatLine(content[i-3]),16)
                    nDataWords = int(formatLine(content[i-2]),16) # Gets nDataWords from trailer
                    trailerSize = nStatusWords+3
                    if not FAIL:
                        histos["inSpy_nWords_ch{0}".format(channel)].Fill(nDataWords) # Fills histogram
                        nGoodEvents= nGoodEvents+1
                        lastL1 = head_l1
                    else:
                        nBadEvents = nBadEvents+1
                        channelFails += 1
                        lastL1=0
                        # inDet doesn't save L1ID to trailer, so no check performed
            if line.startswith("e0f00000"): # Reset at e0f (even if not in event)
                # Reset defaults for next event
                header, data, event = False, False, False
                head_l1,  nWords = 0,0
    # Output useful information
    print "-> Total inSpy Events: ", nGoodEvents+nBadEvents
    print "-> Number of Good Events (all channels): ", nGoodEvents
    print "-> Number of Problem Events (all channels): ", nBadEvents



def IMconsistencyCheck(content):
    """ Checks for consistency in IM formatted data (outSpy) """
    header, data, trailer,event,channelActive = False, False, False,False,False
    head_l1, trail_l1, nDataWords, lastL1 = 0,0,0,-1
    nGoodEvents,nBadEvents, channelFails = 0,0,0
    channel =0
    print "-----Ch 0-----"

    for i,line in enumerate(content):
        if "[FTK_IM" in line and "outspy" in line:
            thisChannel = line.split("outspy")[1].split()[0][2:][:-1]

            # Conditions if we move to a new channel
            if channel != int(thisChannel):
                if not channelActive: print "Channel Inactive"
                if channelActive: print "Number of issues in channel: ", channelFails
                event,channelActive = False,False
                channelFails = 0
                channel +=1
                lastL1 = -1
                print "-----Ch {0}-----".format(channel)


            line = line.split("-")[1].split()[0][2:] # Skim extra contents out of line
            FAIL = False

            # Check if a channel is active based on *any* nonzero events
            if channelActive is not True and not "00000000" in line: channelActive = True

            if line.startswith("b0f00000"): # start of event
                nDataWords,data,trailer = 0,False,False
                if event == True: # Throw error statements if already in an event (no fail, start new event here)
                    print "b0f came in middle of event!"
                    print "    line number: ", i,"\n    l1ID: ", head_l1
                header, event = True, True
                headerWordSeen = 0
                head_l1 =  formatLine(content[i+3]) # Get header l1id based on position wrt b0f
            if header:
                data = False
                headerWordSeen +=1
                if headerWordSeen is 8:
                    header,data = False,True
            if event:
                if not line.startswith("e0da") and data:
                    nDataWords+=1 # Add nWord for every word in data

                if line.startswith("e0da"):
                    data, trailer = False, True
                    trail_l1 =  formatLine(content[i+1]) # get trailer l1id

                if line.startswith("e0f00000"): # End of Event

                    # Check l1id's exist
                    if not head_l1 and trail_l1: print "Trailer l1id but no header"
                    if  head_l1 and not trail_l1: print "Header l1id but no trailer"
                    if head_l1 and trail_l1:
                        if head_l1 == trail_l1: # Good event case
                            FAIL = idIncCheck(lastL1,head_l1) # Fails if l1id doesn't increase right, doesn't fail if previous was bad
                            lastL1 = head_l1
                        if head_l1 != trail_l1:
                            print "Header L1ID does not match trailer.\n    header l1:  ", head_l1, "\n    trailer l1: ", trail_l1
                            FAIL = True
                            lastL1 = 0
                        if not FAIL:
                            histos["outSpy_nWords_ch{0}".format(channel)].Fill(nDataWords) # Fills histogram
                            nGoodEvents= nGoodEvents+1
                        else:
                            nBadEvents = nBadEvents+1
                            channelFails += 1


            if line.startswith("e0f00000"): # Reset at e0f (even if not in event)
                header, data, trailer, event = False, False, False, False
                head_l1, trail_l1, nDataWords = 0,0,0
    # output useful information
    print "-> Total outspy events: ", nGoodEvents+nBadEvents
    print "-> Number of Good Events (all channels): ", nGoodEvents
    print "-> Number of Problem Events (all channels): ", nBadEvents



def outTVcheck(content):
    """ Checks for consistency in Test Vector """
    nWords = TH1F("nWords","",501,0,500)
    header, data, trailer,event = False, False, False,False
    head_l1, trail_l1, nDataWords, lastL1 = 0,0,0,-1
    nGoodEvents,nBadEvents = 0,0

    for i,line in enumerate(content):
        FAIL = False

        if line.startswith("b0f00000"):
            if event == True:
                "b0f came in middle of event!"
            header,event = True,True  # We want to start an event here
            headerWordSeen = 0
            head_l1 =  content[i+3]
            if header:
                headerWordSeen +=1
            if headerWordSeen is 8:
                header,data = False,True
                print data
            if event:
                print "test1"
                print line
                print data
                if not line.startswith("e0da") and data:
                    print "test2"
                    nDataWords = nDataWords+ 1

                if line.startswith("e0da"):
                    print "test3"
                    data, trailer = False, True
                    trail_l1 =  formatLine(content[i+1])


                if line.startswith("b0f00000") and data:
                    print "b0f comes before end of event!"
                    print i
                    FAIL = True

                if line.startswith("e0f00000"):
                    if not head_l1 and trail_l1: print "Trailer but no header"
                    if  head_l1 and not trail_l1: print "Header, no trailer"
                    if head_l1 and trail_l1:
                        if head_l1 == trail_l1: # Good event case
                            FAIL = idIncCheck(lastL1,head_l1)   #fails if l1id doesn't increase right, not fail if previous was bad
                            if nDataWords > 1: nWords.Fill(nDataWords)
    #                        else: print "0 words. i = ",i," line ",line
                            lastL1 = head_l1
                        if head_l1 != trail_l1:
                            print "Header L1ID does not match trailer. \n    header l1:  ", head_l1, "\n    trailer l1: ", trail_l1
                            FAIL = True
                            lastL1 = 0


            if line.startswith("e0f00000"):
                if not FAIL: nGoodEvents= nGoodEvents+1
                else: nBadEvents = nBadEvents+1
                # Reset defaults for next event
                header, data, trailer, event = False, False, False, False
                head_l1, trail_l1, nDataWords = 0,0,0

    print "Total outspy events: ", nGoodEvents+nBadEvents
    print "Number of Good Events: ", nGoodEvents
    print "Number of Problem Events: ", nBadEvents
    outfile = TFile('TVmonitorHistos.root','recreate')
    outfile.cd()
    nWords.Write()

def writeHistos():
    """ Writes histograms, """
    outfile = TFile('MonitorHistos.root','recreate')
    for key in histos:
        histos[key].Write()


    c1= TCanvas()
    ROOT.gErrorIgnoreLevel= kFatal
    gStyle.SetOptStat(1111111)
    for channel in range(15):
        if histos["inSpy_nWords_ch"+str(channel)].GetEntries() > 0:
            histos["inSpy_nWords_ch"+str(channel)].GetXaxis().SetTitle("nWords")
            histos["inSpy_nWords_ch"+str(channel)].Draw()
            if channel == 0: c1.Print(args.outputPDF+"(")
            else: c1.Print(args.outputPDF)
    for channel in range(15):
        if histos["outSpy_nWords_ch"+str(channel)].GetEntries() > 0:
            lastChannel = channel
    for channel in range(15):
        if histos["outSpy_nWords_ch"+str(channel)].GetEntries() > 0:
            histos["outSpy_nWords_ch"+str(channel)].GetXaxis().SetTitle("nWords")
            histos["outSpy_nWords_ch"+str(channel)].Draw()
            if channel == lastChannel: c1.Print(args.outputPDF+")")
            else: c1.Print(args.outputPDF)




if __name__ in '__main__':

    content = f.readlines()

    spyBuffer = False

    for line in content:
        line = line.replace(" ","") #clear any whitespace
        if len(line) > 9:  spyBuffer = True # TVs have only 8 characters, else spybuffer

    if spyBuffer:
        print "=-=-=-=-=-=- INSPY -=-=-=-=-=-="
        inDetConsistencyCheck(content)
        print "=-=-=-=-=-=- OUTSPY -=-=-=-=-=-="
        IMconsistencyCheck(content)
        writeHistos()
    elif not spyBuffer:
        print "You've input a Test Vector!"
        outTVcheck(content)
